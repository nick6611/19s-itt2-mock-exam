#!/usr/bin/env python3

import serial
import time
import yaml

conf_file = "conf.yml"

if __name__ == "__main__":
    with open(conf_file, 'r') as stream:
        serial_cfg = yaml.safe_load(stream)

    print( "Running port {}".format( serial_cfg['device'] ) )

    with serial.Serial(serial_cfg['device'], serial_cfg['baud'], timeout=1) as ser:
        try:
            print("a")
            ser.write( "a".encode() )
            reply = ser.readline().decode()   # read a '\n' terminated line
            print("- {}".format(reply))

            print("zzzz")
            time.sleep(1)

            print("b")
            ser.write( "b".encode() )
            reply = ser.readline().decode()   # read a '\n' terminated line
            print( "- {}".format(reply))

        except KeyboardInterrupt:
            print('interrupted!')

