#define F_CPU 16000000UL


//includes
#include <avr/io.h>
#include <util/delay.h>
 

//definitions

#define BAUD 9600
#define BAUDRATE 103

void uart_init (void);
void UART_Send (unsigned char send);
unsigned char UART_receive (void);

int main(void)
{
	DDRB = 0x10;
	uart_init();
	
	unsigned char receive_data = '0';
	
	
    /* Replace with your application code */
    while (1) 
    {
		
		receive_data = UART_receive();
		
		if ((receive_data == 'a'))
		{
			PORTB |= 0x10;
			UART_Send('1');
			UART_Send('\r');
			UART_Send('\n');
				
		}
		else if ((receive_data == 'b'))
		{
			UART_Send('0');
			UART_Send('\r');
			UART_Send('\n');
			PORTB &= ~0x10;
			
			
		}
		else
		{
			if (receive_data != '\r')
			{
			  UART_Send('E');
			  UART_Send('\r');
			  UART_Send('\n');
			}

		}
		
		
		
    }
}

void uart_init()
{
	UBRR0H = (BAUDRATE >> 8);
	UBRR0L = BAUDRATE;
	UCSR0B |= (1<<TXEN0) | (1<<RXEN0);
	UCSR0C |= (3<<UCSZ00);
}


void UART_Send(unsigned char send)
{
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = send;
	
}

unsigned char UART_receive(void)
{
	while (!(UCSR0A & (1<<RXC0)));
	return UDR0;

}